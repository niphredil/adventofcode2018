filename: str = "input.txt"

with open(filename) as f:
    content: [str] = [line.strip() for line in f.readlines()]  # load file to [str] and strip newlines


def process_line(line: str) -> (bool, bool):
    double, triple = False, False
    for c in set(line):
        c_count: int = line.count(c)
        if c_count == 2:
            double = True
        elif c_count == 3:
            triple = True
    return double, triple


no_of_lines_with_duplicate, no_of_lines_with_triplicate = 0, 0
for line in content:
    d, t = process_line(line)
    if d:
        no_of_lines_with_duplicate += 1
    if t:
        no_of_lines_with_triplicate += 1

checksum: int = no_of_lines_with_duplicate * no_of_lines_with_triplicate

print("Checksum: " + str(checksum))
