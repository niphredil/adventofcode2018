from difflib import SequenceMatcher

filename: str = "input.txt"

with open(filename) as f:
    content: [str] = [line.strip() for line in f.readlines()]  # load file to [str] and strip newlines


def find_matching_block(s1: str, s2: str) -> str:
    matcher = SequenceMatcher(None, s1, s2)
    similarity: int = matcher.ratio()
    desired_similarity: int = (len(s1) - 1) / len(s1)  # difference of exactly 1 character
    if similarity == desired_similarity:
        matching_block: str = ""
        for i, c in enumerate(s1):
            if c == s2[i]:
                matching_block += c
        return matching_block
    else:
        return ""


def find_strings_with_one_difference_and_return_matching_part(lines: [str]) -> str:
    for i, first_line in enumerate(lines):
        for second_line in lines[i+1:]:  # avoid comparing lines twice
            matching_block: str = find_matching_block(first_line, second_line)
            if not matching_block:
                continue
            else:
                return matching_block


print("Matching block: " + find_strings_with_one_difference_and_return_matching_part(content))
