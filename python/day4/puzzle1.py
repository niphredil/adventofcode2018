import re
from datetime import datetime
from enum import Enum
from typing import List, Dict

FILENAME: str = "input.txt"
GuardId = int


class EventType(Enum):
    BEGIN_SHIFT = 1
    FALL_ASLEEP = 2
    WAKE_UP = 3


class Guard:
    def __init__(self, guard_id: int, total_sleep: int = 0):
        self.id = guard_id
        self.total_sleep = total_sleep
        self.sleep_per_minute = {x: 0 for x in range(0, 60)}

    def sleep(self, minute: int):
        self.sleep_per_minute[minute] += 1
        self.total_sleep += 1


class Event:
    def __init__(self, time: datetime, guard_id: GuardId, event_type: EventType):
        self.time = time
        self.guard_id = guard_id
        self.event_type = event_type


def parse_line(line: str) -> Event:
    guard_id: int = 0
    event: EventType
    time: datetime = datetime.strptime(line[1:17], "%Y-%m-%d %H:%M")
    guard_id_raw = re.search("#(\d+) ", line)
    if guard_id_raw:
        guard_id = GuardId(guard_id_raw.group(1))  # else it must be read from preceding record
    if re.search("begins", line):
        event = EventType.BEGIN_SHIFT
    elif re.search("asleep", line):
        event = EventType.FALL_ASLEEP
    elif re.search("wake", line):
        event = EventType.WAKE_UP

    return Event(time, guard_id, event)


def calculate_sleep_time_per_guard(events: List[Event]) -> Dict[GuardId, Guard]:
    guards: Dict[GuardId, Guard] = {}
    current_guard_id: GuardId = 0
    for i, event in enumerate(events):
        if event.event_type == EventType.BEGIN_SHIFT:  # guard change (shift beginning)
            current_guard_id = GuardId(event.guard_id)
            if current_guard_id not in guards:
                guards[current_guard_id] = Guard(current_guard_id)
        elif event.event_type == EventType.FALL_ASLEEP:  # we are only interested in interval [asleep, shift/wake)
            if event == events[-1]:  # check that we are not at the end of list
                break
            else:
                next_event: Event = events[
                    i + 1]  # next event should be begin_shift or wake_up (can´t fall asleep twice)
                one_am: datetime = datetime(1518, event.time.month, event.time.day, hour=1)
                if next_event.time > one_am:
                    sleep_duration = (one_am - event.time).seconds // 60  # in minutes
                else:
                    sleep_duration = (next_event.time - event.time).seconds // 60  # in minutes
                sleep_first_minute: int = event.time.minute
                for minute in range(sleep_first_minute, sleep_first_minute + sleep_duration):
                    minute_hourly: int = minute % 60
                    guards[current_guard_id].sleep(minute_hourly)
    return guards


def main():
    with open(FILENAME) as f:
        events: List[Event] = [parse_line(line) for line in f.readlines()]
        events.sort(key=lambda x: x.time)

    guards: Dict[GuardId, Guard] = calculate_sleep_time_per_guard(events)
    guards_sorted: Dict[GuardId, Guard] = sorted(guards.values(), key=lambda g: g.total_sleep, reverse=True)
    # minute on which the laziest guard sleeps the most often
    laziest_minute: int = sorted(guards_sorted[0].sleep_per_minute.items(), key=lambda t: t[1], reverse=True)[0][0]
    laziest_guard_id = guards_sorted[0].id
    solution: int = laziest_guard_id * laziest_minute

    print("Solution is: " + str(solution))


if __name__ == '__main__':
    main()
