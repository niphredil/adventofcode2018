from typing import List, Tuple


FILENAME: str = "input.txt"

Index = int


class Node:
    children: List['Node']
    metadata: List[int]

    def __init__(self):
        self.children = []
        self.metadata = []

    def append_metadata(self, metadata: int):
        self.metadata.append(metadata)

    def append_child(self, child: 'Node'):
        self.children.append(child)


def read_node(node: Node, data: List[int], index: Index) -> Tuple[Node, Index]:
    no_of_children: int = data[index]
    no_of_metadata: int = data[index + 1]
    next_child_index: Index = index + 2  # initialize - case when node has no children
    for ch in range(0, no_of_children):
        child: Tuple[Node, Index] = read_node(Node(), data, next_child_index)
        node.append_child(child[0])
        next_child_index = child[1]
    for m in range(0, no_of_metadata):
        node.append_metadata(data[next_child_index + m])
    return node, next_child_index + no_of_metadata


def build_tree(data: List[int]) -> Node:
    initial_index: Index = 0  # track where you are in the sequence
    empty_node: Node = Node()
    root: Node = read_node(empty_node, data, initial_index)[0]
    return root


def calculate_value(node: Node) -> int:
    if node.children:
        running_total: int = 0
        for m in node.metadata:
            try:
                running_total += calculate_value(node.children[m-1])
            except IndexError:  # child corresponding to metadata index does not exist -> skip
                pass
        return running_total
    else:
        return sum(node.metadata)


def main():
    with open(FILENAME) as f:
        data: List[int] = [int(x) for x in f.readline().split()]

    tree: Node = build_tree(data)
    tree_value: int = calculate_value(tree)

    print("Tree value: " + str(tree_value))


if __name__ == '__main__':
    main()
