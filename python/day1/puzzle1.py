filename: str = "puzzle1_input.txt"

with open(filename) as f:
    raw_content: [str] = f.readlines()

content: [int] = [int(line.strip()) for line in raw_content]  # strip newlines and convert to [int]

frequency_offset: int = sum(content)
print("Final frequency: " + str(frequency_offset))
