filename: str = "puzzle1_input.txt"

with open(filename) as f:
    raw_content: [str] = f.readlines()

content: [int] = [int(line.strip()) for line in raw_content]  # strip newlines and convert to [int]


def get_first_repeated_frequency(frequency_list: [int]) -> int:
    frequency_offset: int = 0
    visited_frequencies: [int] = [frequency_offset]

    while True:  # it might take multiple loops to get an answer
        for frequency in frequency_list:
            frequency_offset += frequency
            if frequency_offset in visited_frequencies:
                return frequency_offset
            else:
                visited_frequencies.append(frequency_offset)


print("First repeated frequency: " + str(get_first_repeated_frequency(content)))
