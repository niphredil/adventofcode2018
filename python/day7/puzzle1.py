from typing import List, Dict

FILENAME = "input.txt"


StepName = str
PreReq = str


def read_steps(lines: List[str]) -> Dict[StepName, List[PreReq]]:
    steps: Dict[StepName, List[PreReq]] = {}
    for line in lines:
        step_name, prerequisite = line[36], line[5]
        if prerequisite not in steps:  # prerequisite can be added unconditionally
            steps[prerequisite] = []
        if step_name not in steps:
            steps[step_name] = [prerequisite]
        else:  # step already exists, just add a prerequisite
            if prerequisite not in steps[step_name]:
                steps[step_name].append(prerequisite)
    return steps


def next_step(steps: Dict[StepName, List[PreReq]]) -> StepName:
    possible_steps = [s[0] for s in steps.items() if s[1] == []]
    return sorted(possible_steps)[0]  # first alphabetically


def main():
    with open(FILENAME) as f:
        content = [line.strip() for line in f.readlines()]

    steps: Dict[StepName, List[PreReq]] = read_steps(content)
    solution: str = ""
    while steps:
        next_step_name = next_step(steps)
        solution += next_step_name
        del steps[next_step_name]

        # need to remove finished step from all prerequisites
        for k, v in steps.items():
            if next_step_name in v:
                steps[k].remove(next_step_name)
    print("Solution: " + solution)


if __name__ == '__main__':
    main()
