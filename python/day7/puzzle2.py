from typing import List, Dict, ValuesView

FILENAME = "input.txt"
WORKER_COUNT = 5
BASELINE_TASK_DURATION = 60


StepName = str
PreReq = str


def read_steps(lines: List[str]) -> Dict[StepName, List[PreReq]]:
    steps: Dict[StepName, List[PreReq]] = {}
    for line in lines:
        step_name, prerequisite = line[36], line[5]
        if prerequisite not in steps:  # prerequisite can be added unconditionally
            steps[prerequisite] = []
        if step_name not in steps:
            steps[step_name] = [prerequisite]
        else:  # step already exists, just add a prerequisite
            if prerequisite not in steps[step_name]:
                steps[step_name].append(prerequisite)
    return steps


def next_step(steps: Dict[StepName, List[PreReq]], task_buffer: ValuesView[List[str]]) -> StepName:
    possible_steps = [s[0] for s in steps.items() if s[1] == [] and s[0] not in task_buffer]
    if possible_steps:
        return sorted(possible_steps)[0]  # first alphabetically
    else:
        return ""


def calculate_task_duration(step_name: StepName) -> int:
    step_duration: int = ord(step_name) - 64 + BASELINE_TASK_DURATION  # A starts at 1
    return step_duration


def main():
    with open(FILENAME) as f:
        content = [line.strip() for line in f.readlines()]

    steps: Dict[StepName, List[PreReq]] = read_steps(content)
    time_elapsed: int = 0
    available_workers: int = WORKER_COUNT
    finished: bool = False
    task_buffer: Dict[int, List[str]] = {}  # time at which task should be completed -> step names

    while not finished:
        next_step_name = next_step(steps, task_buffer.values())
        if available_workers > 0:
            if next_step_name:  # there is a task worker can start doing
                next_step_duration: int = calculate_task_duration(next_step_name)
                if time_elapsed + next_step_duration - 1 in task_buffer:
                    task_buffer[time_elapsed + next_step_duration - 1].append(next_step_name)  # schedule finish time
                else:
                    task_buffer[time_elapsed + next_step_duration - 1] = [next_step_name]
                del steps[next_step_name]
                available_workers -= 1

        if time_elapsed in task_buffer:  # there is a task that can be finished this second
            for finished_step in task_buffer[time_elapsed]:
                # del steps[finished_step]
                for k, v in steps.items():  # need to remove finished step from all prerequisites
                    if finished_step in v:
                        steps[k].remove(finished_step)
                available_workers += 1

                # remove from task buffer
                for k, v in task_buffer.items():
                    if k == time_elapsed:
                        if len(v) == 1:  # only one task remains to be finished right now
                            del task_buffer[time_elapsed]
                            break
                        else:
                            task_buffer[time_elapsed].remove(finished_step)
                            break

        # check if we can move one second forward or if all steps are finished
        if not steps and not task_buffer:
            finished = True
        else:
            if available_workers == 0 or not next_step_name:  # we lack workers or steps lack prerequisites
                time_elapsed += 1

    print("Total time elapsed: " + str(time_elapsed + 1))


if __name__ == '__main__':
    main()
