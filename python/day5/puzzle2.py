import regex

FILENAME = "input.txt"

with open(FILENAME) as f:
    content = ''.join([line.strip() for line in f.readlines()])


def should_be_removed(match):
    if match.group(0)[0] == match.group(0)[1]:  # case matches, do not remove
        return False
    else:
        return True


def remove_unit(unit: str, polymer: str) -> str:
    """Generate new polymer by removing both lower/uppercase versions of a single letter."""
    return regex.sub(unit, "", polymer, flags=regex.IGNORECASE)


def find_first_match(polymer):
    matches = regex.finditer(r'([a-z])\1', polymer, overlapped=True, flags=regex.IGNORECASE)
    for match in matches:
        if should_be_removed(match):
            return match
        else:
            continue
    return None


def process_polymer(polymer: str) -> str:
    polymer_is_fully_processed = False
    while not polymer_is_fully_processed:
        first_match = find_first_match(polymer)
        if first_match:
            replace_start = first_match.span(0)[0]
            replace_end = first_match.span(0)[1]
            polymer = polymer[:replace_start] + polymer[replace_end:]
        else:
            polymer_is_fully_processed = True  # no match found, we are done
    return polymer


def find_shortest_polymer_length(polymer: str) -> int:
    polymer_lengths = {}
    for letter in [chr(code) for code in range(97, 123)]:  # iterate over a-z
        polymer_without_single_letter = remove_unit(letter, polymer)
        processed_polymer = process_polymer(polymer_without_single_letter)
        polymer_lengths[letter] = len(processed_polymer)
    shortest_polymer_length = polymer_lengths[sorted(polymer_lengths, key=polymer_lengths.get)[0]]
    return shortest_polymer_length


print("Length of final polymer: " + str(find_shortest_polymer_length(content)))
