import regex

FILENAME = "input.txt"

with open(FILENAME) as f:
    content = ''.join([line.strip() for line in f.readlines()])


def should_be_removed(match):
    if match.group(0)[0] == match.group(0)[1]:  # case matches, do not remove
        return False
    else:
        return True


def find_first_match(polymer):
    matches = regex.finditer(r'([a-z])\1', polymer, overlapped=True, flags=regex.IGNORECASE)
    for match in matches:
        if should_be_removed(match):
            return match
        else:
            continue
    return None


def process_polymer(polymer: str) -> str:
    polymer_is_fully_processed = False
    while not polymer_is_fully_processed:
        first_match = find_first_match(polymer)
        if first_match:
            replace_start = first_match.span(0)[0]
            replace_end = first_match.span(0)[1]
            polymer = polymer[:replace_start] + polymer[replace_end:]
        else:
            polymer_is_fully_processed = True  # no match found, we are done
    return polymer


print("Length of final polymer: " + str(len(process_polymer(content))))
