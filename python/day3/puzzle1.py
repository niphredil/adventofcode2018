import re
from typing import Sequence, List

FILENAME: str = "input.txt"


class Rectangle:

    # used coordinate system:
    # (0, 0) is at top left corner, x increases to the right, y increases down
    def __init__(self, left_margin: int, top_margin: int, width: int, height: int):
        self.x1 = left_margin           # top left corner
        self.y1 = top_margin        # top left corner
        self.x2 = self.x1 + width   # bottom right corner
        self.y2 = self.y1 + height  # bottom right corner

    # def do_overlap(self, other: 'Rectangle') -> bool:
    #     do_x_overlap: bool = self.x1 < other.x1 < self.x2 or self.x1 < other.x2 < self.x2
    #     if not do_x_overlap:
    #         return False
    #     do_y_overlap: bool = self.y1 < other.y1 < self.y2 or self.y1 < other.y2 < self.y2
    #     if not do_y_overlap:
    #         return False
    #     else:
    #         return True


def parse_line(line: str) -> Rectangle:
    # get a list of integers, excluding delimiters and whitespace
    int_list: Sequence[int] = [int(p) for p in filter(None, re.split("[#@:,x ]", line))]
    return Rectangle(int_list[1], int_list[2], int_list[3], int_list[4])  # ignore first param (ID)


def create_canvas() -> List[List[int]]:
    canvas_width, canvas_height = 1000, 1000
    new_canvas = [[0 for _ in range(canvas_width)] for _ in range(canvas_height)]  # initialize all square inches to 0
    return new_canvas

with open(FILENAME) as f:
    rectangles: Sequence[Rectangle] = [parse_line(line.strip()) for line in f.readlines()]

canvas: List[List[int]] = create_canvas()
for rectangle in rectangles:
    for x in range(rectangle.x1, rectangle.x2):
        for y in range(rectangle.y1, rectangle.y2):
            canvas[x][y] += 1

# get only square inches with multiple claims
filtered_list: Sequence[Sequence[int]] = [[sqi for sqi in sublist if sqi > 1] for sublist in canvas]
sqi_with_multiple_claims: int = sum(len(sublist) for sublist in filtered_list)
print("Number of square inches with multiple claims: " + str(sqi_with_multiple_claims))

