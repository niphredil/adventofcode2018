import re
from typing import Sequence, List

FILENAME: str = "input.txt"


class Rectangle:
    # used coordinate system:
    # (0, 0) is at top left corner, x increases to the right, y increases down
    def __init__(self, claim_id: int, left_margin: int, top_margin: int, width: int, height: int):
        self.claim_id = claim_id
        self.x1 = left_margin  # top left corner
        self.y1 = top_margin  # top left corner
        self.x2 = self.x1 + width  # bottom right corner
        self.y2 = self.y1 + height  # bottom right corner


def parse_line(line: str) -> Rectangle:
    # get a list of integers, excluding delimiters and whitespace
    params: Sequence[int] = [int(p) for p in filter(None, re.split("[#@:,x ]", line))]
    return Rectangle(params[0], params[1], params[2], params[3], params[4])


def create_canvas() -> List[List[int]]:
    canvas_width, canvas_height = 1000, 1000
    new_canvas = [[0 for _ in range(canvas_width)] for _ in range(canvas_height)]  # initialize all square inches to 0
    return new_canvas


def is_rectangle_without_overlap(r: Rectangle) -> bool:
    for rx in range(r.x1, r.x2):
        for ry in range(r.y1, r.y2):
            if canvas[rx][ry] is not 1:  # early return, square inch has multiple claims
                return False
    return True


with open(FILENAME) as f:
    rectangles: Sequence[Rectangle] = [parse_line(line.strip()) for line in f.readlines()]

canvas: List[List[int]] = create_canvas()
# first pass through -> mark each square inch by number of claims
for rectangle in rectangles:
    for x in range(rectangle.x1, rectangle.x2):
        for y in range(rectangle.y1, rectangle.y2):
            canvas[x][y] += 1

# second pass through -> find the one claim for which all squares have value == 1 (no overlap with others)
for rectangle in rectangles:
    if is_rectangle_without_overlap(rectangle):
        print("Found square without overlap, its ID is: " + str(rectangle.claim_id))
