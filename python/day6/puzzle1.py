from typing import List
import operator

FILENAME = "input.txt"


class Vector:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    def distance(self, other: 'Vector') -> int:
        if other:
            return abs(self.x - other.x) + abs(self.y - other.y)  # Manhattan distance
        else:
            return 1000  # vector not defined

    def get_closest_point(self, vectors: List['Vector']):
        closest_vector, is_tie = None, False
        for v in vectors:
            distance_to_current = self.distance(v)
            distance_to_previous_closest = self.distance(closest_vector)
            if distance_to_current < distance_to_previous_closest:
                closest_vector, is_tie = v, False
            elif distance_to_current == distance_to_previous_closest:  # if there is a tie, no vector is closest
                is_tie = True
        if not is_tie:
            return closest_vector
        else:
            return None


def read_lines(lines: List[str]) -> (List[Vector], int, int, int, int):
    vl, x_min, x_max, y_min, y_max = [], 1000, -1000, 1000, -1000
    for line in lines:
        split_line = line.split(r', ')
        vx, vy = int(split_line[0]), int(split_line[1])

        # we are interested in coordinates fencing in all our points
        if vx < x_min:
            x_min = vx
        if vx > x_max:
            x_max = vx
        if vy < y_min:
            y_min = vy
        if vy > y_max:
            y_max = vy

        vl.append(Vector(vx, vy))
    return vl, x_min, x_max, y_min, y_max


def main():
    with open(FILENAME) as f:
        content = [line.strip() for line in f.readlines()]

    data_raw = read_lines(content)
    points = data_raw[0]
    point_areas, point_areas_inc = {}, {}
    x_min, x_max, y_min, y_max = data_raw[1], data_raw[2], data_raw[3], data_raw[4]

    # first pass
    for x in range(x_min - 1, x_max + 1):
        for y in range(y_min - 1, y_max + 1):
            closest_point = Vector(x, y).get_closest_point(points)
            if closest_point:
                if closest_point in point_areas:
                    point_areas[closest_point] += 1
                else:
                    point_areas[closest_point] = 1

    # second pass in a slightly larger area - if vector area changed, it will probably be infinite?
    for x in range(x_min - 2, x_max + 2):
        for y in range(y_min - 2, y_max + 2):
            closest_point = Vector(x, y).get_closest_point(points)
            if closest_point:
                if closest_point in point_areas_inc:
                    point_areas_inc[closest_point] += 1
                else:
                    point_areas_inc[closest_point] = 1

    # filter point_areas so that only vectors whose areas did not change remain
    final_point_areas = {}
    for key, value in point_areas.items():
        if point_areas[key] == point_areas_inc[key]:
            final_point_areas[key] = value
    largest_finite_area = sorted(final_point_areas.items(), key=operator.itemgetter(1), reverse=True)[0][1]
    print("Largest finite area: " + str(largest_finite_area))


if __name__ == '__main__':
    main()
