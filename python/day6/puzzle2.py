from typing import List, Tuple
import operator

FILENAME = "input.txt"


class Vector:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    def distance(self, other: 'Vector') -> int:
        if other:
            return abs(self.x - other.x) + abs(self.y - other.y)  # Manhattan distance
        else:
            return 1000  # vector not defined

    def get_closest_point(self, vectors: List['Vector']):
        closest_vector, is_tie = None, False
        for v in vectors:
            distance_to_current = self.distance(v)
            distance_to_previous_closest = self.distance(closest_vector)
            if distance_to_current < distance_to_previous_closest:
                closest_vector, is_tie = v, False
            elif distance_to_current == distance_to_previous_closest:  # if there is a tie, no vector is closest
                is_tie = True
        if not is_tie:
            return closest_vector
        else:
            return None


def read_lines(lines: List[str]) -> (List[Vector], int, int, int, int):
    vl, x_min, x_max, y_min, y_max = [], 1000, -1000, 1000, -1000
    for line in lines:
        split_line = line.split(r', ')
        vx, vy = int(split_line[0]), int(split_line[1])

        # we are interested in coordinates fencing in all our points
        if vx < x_min:
            x_min = vx
        if vx > x_max:
            x_max = vx
        if vy < y_min:
            y_min = vy
        if vy > y_max:
            y_max = vy

        vl.append(Vector(vx, vy))
    return vl, x_min, x_max, y_min, y_max


def get_total_distance(point: Vector, other_points: List[Vector]) -> int:
    distance: int = 0
    for p in other_points:
        distance += p.distance(point)
    return distance


def main():
    with open(FILENAME) as f:
        content = [line.strip() for line in f.readlines()]

    data_raw: Tuple[List[Vector], int, int, int, int] = read_lines(content)
    points: List[Vector] = data_raw[0]
    x_min, x_max, y_min, y_max = data_raw[1], data_raw[2], data_raw[3], data_raw[4]
    safe_area_size: int = 0

    # first pass
    for x in range(x_min - 1, x_max + 1):
        for y in range(y_min - 1, y_max + 1):
            total_distance = get_total_distance(Vector(x, y), points)
            if total_distance < 10000:
                safe_area_size += 1

    print("Safe area size: " + str(safe_area_size))


if __name__ == '__main__':
    main()
